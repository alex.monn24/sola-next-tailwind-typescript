
const CreateRoomButton = () => {
    return (
        <div className=" flex justify-center items-center bg-primary w-[124px] h-[52px] rounded-[15px]
                        font-['Outfit'] font-[500] text-[16px] text-[#f3f3f3] ">
            Create Room
        </div>
    )
}

export default CreateRoomButton