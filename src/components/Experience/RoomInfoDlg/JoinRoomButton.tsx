import React from "react"

const JoinRoomButton = () => {
    return (
        <div className=" flex flex-row justify-center items-center bg-[#29b080] rounded-[15px] h-[52px] w-[104px]
                        text-white font-['Outfit'] font-[500] text-[16px] text-[#f3f3f3] cursor-pointer">
            Join room
        </div>
    )
}

export default JoinRoomButton