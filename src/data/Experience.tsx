
import { SolanaIcon } from "components/icons"
import { ExploreRoomDataType, LiveRoomListDataType, RoomMemberAvatarSmallDataType } from "modal/experience"

export const RoomMemberAvatarSmallData : RoomMemberAvatarSmallDataType [] = [
    {imgUrl : ""},
    {imgUrl : "/images/experience/room_user_avatars/room_user_avatar_1.webp"},
    {imgUrl : "/images/experience/room_user_avatars/room_user_avatar_2.webp"},
    {imgUrl : "/images/experience/room_user_avatars/room_user_avatar_2.webp"},
    {imgUrl : "/images/experience/room_user_avatars/room_user_avatar_4.webp"},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},
    {imgUrl : ""},

]

export const LiveRoomListData : LiveRoomListDataType[] = [
    {
        currentNumberOfMembers : 5,
        imgUrl : "/images/experience/room_images/room_1_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "CollectionName",
        roomName : "The Vincent Van Dough Gallery",
    },
    {
        currentNumberOfMembers : 23,
        imgUrl : "/images/experience/room_images/room_2_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "CollectionName",
        roomName : "Teufzer",
    },
    {
        currentNumberOfMembers : 10,
        imgUrl : "/images/experience/room_images/room_3_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "CollectionName",
        roomName : "RESSURECTION enriched with death #244314213123123",
    },
    {
        currentNumberOfMembers : 144,
        imgUrl : "/images/experience/room_images/room_4_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Solana Money Boys",
        roomName : "Meta trap house",
    },
    {
        currentNumberOfMembers : 33,
        imgUrl : "/images/experience/room_images/room_5_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "CollectionName",
        roomName : "This room is really fantastic",
    },
    {
        currentNumberOfMembers : 9,
        imgUrl : "/images/experience/room_images/room_6_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "what do you want?",
        roomName : "Dummy text goes here",
    },
    {
        currentNumberOfMembers : 2,
        imgUrl : "/images/experience/room_images/room_7_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Quiet room",
        roomName : "Leave me alone",
    },
    {
        currentNumberOfMembers : 36,
        imgUrl : "/images/experience/room_images/room_8_avatar_sm.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "So Crowd",
        roomName : "The Vincent Van Dough Gallery",
    },
]

export const ExploreRoomData : ExploreRoomDataType[] =[
    {
        imgUrl : "/images/experience/room_images/room_8_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "6529 Photo A"
    },
    {
        imgUrl : "/images/experience/room_images/room_7_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "Justin Aversano’s Twin Flames"
    },
    {
        imgUrl : "/images/experience/room_images/room_6_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "CyberKongz"
    },
    {
        imgUrl : "/images/experience/room_images/room_5_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "Dummy text"
    },
    {
        imgUrl : "/images/experience/room_images/room_4_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "9438 Photo C"
    },
    {
        imgUrl : "/images/experience/room_images/room_3_avatar_lg.jpg",
        walletIcon : <SolanaIcon />,
        collectionName : "Collection name",
        roomName : "6529 Photo A"
    },
]