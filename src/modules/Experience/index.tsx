import React from "react"
import CreateRoomSection from "./CreateRoomSection"

const Experience = ({sidebarToggler}) => {
    return (
        <div className=" flex flex-row mt-[39px] ">
            <CreateRoomSection />
        </div>
    )
}

export default Experience